const gameContainer = document.getElementById("game");

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple"
];


function shuffle(array) {
  let counter = array.length;
  while (counter > 0) {
    let index = Math.floor(Math.random() * counter);
    counter--;
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}


let shuffledColors = shuffle(COLORS);



var memory_values = [];
var memory_tile_ids = [];
var tiles_flipped = 0;


function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    const newDiv = document.createElement("div");
    newDiv.classList.add(color);
    newDiv.addEventListener("click", handleCardClick);
    gameContainer.append(newDiv);
  }
}

// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked
  console.log("you pallabi clicked",event.target.className);
  
  if(event.target.style.background == "" && memory_values.length < 2){
  event.target.style.background = event.target.className;
   if(memory_values.length == 0){
   memory_values.push(event.target.className);
   memory_tile_ids.push(event.target);
  } else if(memory_values.length == 1){
  memory_values.push(event.target.className);
  memory_tile_ids.push(event.target);
  if(memory_values[0] == memory_values[1]){
                    tiles_flipped += 2;
                    memory_values = [];
                    memory_tile_ids = [];
              if(tiles_flipped == COLORS.length){
                    alert("Board cleared... generating new board");
                document.getElementById('game').innerHTML = "";
                createDivsForColors();
              }
            } 


			else
      {
				function flip2Back(){
				    memory_tile_ids[0].style.background = "";
          memory_tile_ids[1].style.background = "";
				    
				    memory_values = [];
			memory_tile_ids = [];
				}
				setTimeout(flip2Back, 700);
			}
  }
  }}

// when the DOM loads
createDivsForColors(shuffledColors);
